package com.company.XMLSerializer;

@XmlObject
public class TestClass {
    @XmlTag
    public Integer x;

    @XmlAttribute
    private Integer y;

    @XmlTag
    public TestClass2 k;

    public TestClass(){
        x = 5;
        y = 3;
        k = new TestClass2();
    }
}
