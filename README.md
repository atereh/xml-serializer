### XML Serializer:

**Task:** 
Write your own XML serializer on annotations.
The @XmlObject annotation marks a class that can be serialized in XML. 
Use the @XmlTag annotation to mark a field that should be serialized as a separate tag, or a method whose result should be serialized as a tag.
Use the @XmlAttribute annotation to mark the field (or method) that should be serialized to an XML attribute

### Suggested restrictions:

* All annotations must be able to redefine the tag / attribute name. If not redefined, the name of the class/field/method stays the name. If the method name starts with *get*, this get is discarded

* There is no difference which access level fields and methods have with @XmlTag or @XmlAttribute annotations

* A method can only be marked with the @XmlTag or @XmlAttribute annotation if it has no parameters. If there are, we throw an exception.
  Also, a tag or an attribute can't be a method that returns void.

* If two tags with the same name are obtained at the same level, or two attributes with the same name appear in the same tag, we throw an exception

* @XmlAttribute must be able to specify the name of the tag that the attribute belongs to. If the tag name is not specified, the attribute belongs to the root tag of the class

* If a field marked by @XmlTag (or the result of a method marked by @XmlTag) is an object with the @XmlObject annotation, the entire serialization result of this object is transferred to this tag. Otherwise, we just use *toString ()* as the value. (you can make it more complex and better, but it's enough to embed one xml in another)

* For @XmlAttribute, we always use *toString()*

* If the parent class also has the @XmlObject annotation, we add all its tags and attributes to the current xml. 
  If it doesn't, we ignore it

* For building XML (creating tags and attributes), you can use third-party libraries (for example, DOM4j). 
  If desired, you can build XML manually. In any case, it is **forbidden to use** annotations and mappers from third-party libraries

### Contributors:
Alina Terekhova